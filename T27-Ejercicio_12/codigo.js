function escapeHTML(text) {

    var replacements = [["&", "&amp;"], ["\"","&quot;"], ["<",  "&lt;"], [">", "&gt;"]];

    forEach(replacements, function(replace) {
        text = text.replace(replace[0], replace[1]);
    });

    return text;

}